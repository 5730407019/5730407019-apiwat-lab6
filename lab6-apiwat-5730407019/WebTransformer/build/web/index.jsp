<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Catalog Transformation by Apiwat</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <form action ="WebTransformFile" method="get">
            <h2>Catalog Transformation</h2>
            <table>
                <tr>
                    <td>XML:</td> 
                    <td><input name ="inXML" type ="text" size ="80"/></td>
                </tr>
                <tr>
                    <td>XSL:</td>
                    <td><input name ="inXSL" type ="text" size ="80"/></td>
                </tr>
                <tr><td><input name="" type="submit" value="Submit" /></td></tr>

            </table>
        </form>
    </body>
</html>
