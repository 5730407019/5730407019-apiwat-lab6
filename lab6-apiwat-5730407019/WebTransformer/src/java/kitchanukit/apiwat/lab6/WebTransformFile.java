
package kitchanukit.apiwat.lab6;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import jdk.internal.org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;



public class WebTransformFile extends HttpServlet {

   /*
     
     //Figure :7 inlab
     
     protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
              out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Catalog Transformation by Apiwat</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Catalog Transformation Result</h1>");
            String xmlFile = request.getParameter("inXML");
            String xslFile = request.getParameter("inXSL");
            out.print("An input XML file is " + xmlFile +" an intput XSL file is " +xslFile);
            out.println("</body>");
            out.println("</html>");
        }      
    }
    
}*/
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
               
            out.println("<title>Catalog Transformation</title>");            
            out.println("<h1>Catalog Transformation Result </h1>");
            String xmlFile = request.getParameter("inXML");
            String xslFile = request.getParameter("inXSL");
            
            try {
                DocumentBuilderFactory Factory = DocumentBuilderFactory.newInstance();
                Factory.setNamespaceAware(true);
                DocumentBuilder parser = Factory.newDocumentBuilder();
                
                Document document = parser.parse(xmlFile);
                Document xslfile = parser.parse(xslFile);
                Document result = transformXML(document, xslfile);
                
                LSSerializer serializer = ((DOMImplementationLS) document.getImplementation()).createLSSerializer();
                out.println(serializer.writeToString(result));
              
        } catch (Exception ex) {
            System.err.println("There's an error.");
        }      
        }
    }
    public  Document transformXML(Document xml, Document xslt) 
            throws TransformerException, ParserConfigurationException, FactoryConfigurationError {

        Source xmlSource = new DOMSource(xml);
        Source xsltSource = new DOMSource(xslt);
        DOMResult ans = new DOMResult();
        
        TransformerFactory transFact = TransformerFactory.newInstance();
        Transformer trans = transFact.newTransformer(xsltSource);
        trans.transform(xmlSource, ans);
        Document resultDoc = (Document) ans.getNode();

        return resultDoc;
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}