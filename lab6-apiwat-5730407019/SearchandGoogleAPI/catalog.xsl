<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>
    <xsl:template match="catalog">
        <html>
            <style>
                th{
                background-color: yellow;
                }
                td{
                background-color: LightGrey;
                }
            </style>
            <body>
          
                <h2>MY CD Collection</h2>
                <table>
                <tr>
                    <th>CD Title</th>
                </tr>
                <xsl:for-each select= "cd">
                    <tr>
                        <td><xsl:value-of select="title"/></td>
                    </tr>
                </xsl:for-each>
                </table>        
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
